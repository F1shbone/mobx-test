import { mapFields as _mapFields, mapMethods as _mapMethods } from "movue";

import ToDoStore from "./toDoStore";

const toDoStore = new ToDoStore(this, [
  { name: "make store", checked: true },
  { name: "make react app", checked: false },
  { name: "make vue app", checked: false }
]);

const rootStore = {
  toDoStore
};

const _getStore = function(path) {
  let _store = rootStore;
  path.split("/").forEach(pathPart => {
    _store = _store[pathPart];
  });
  return _store;
};

const mapFields = (store, fields) => _mapFields(_getStore(store), fields);
const mapMethods = (store, fields) => _mapMethods(_getStore(store), fields);

export {
  // Stores
  toDoStore,
  // Methods
  mapFields,
  mapMethods
};
export default rootStore;
