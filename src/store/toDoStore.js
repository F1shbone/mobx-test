import * as mobx from "mobx";

const { observable, computed, action } = mobx;

class ToDoStore {
  @observable list = [];
  @observable newItem = "";

  constructor(rootStore, items) {
    this.rootStore = rootStore;

    for (let item of items) {
      this.addItem(item);
    }
  }

  @computed
  get count() {
    return this.list.length;
  }
  @computed
  get checkedCount() {
    let count = 0;

    for (let item of this.list) {
      if (item.checked) {
        count += 1;
      }
    }

    return count;
  }

  @action
  addItem(item) {
    this.list.push({
      id: this.list.length,
      checked: false,
      name: "Unnamed deal",
      ...item
    });
  }
  @action
  addNewItem() {
    if (this.newItem === "") {
      return null;
    }

    this.addItem({ name: this.newItem });
    this.newItem = "";
  }
  @action
  setNewItemValue(value) {
    if (!value) {
      value = "";
    }
    this.newItem = value.toString();
  }
  @action
  checkItem(id) {
    const index = this._getIndexById(id);
    if (index < 0) {
      return null;
    }

    return this.editItem(id, { checked: !this.list[index].checked });
  }
  @action
  editItem(id, item) {
    const index = this._getIndexById(id);
    if (index < 0) {
      return null;
    }

    this.list[index] = {
      ...this.list[index],
      ...item
    };
    return this.list[index];
  }
  @action
  removeItem(id) {
    const index = this._getIndexById(id);
    if (index < 0) {
      return null;
    }

    return this.list.splice(index, 1)[0];
  }

  _getIndexById(id) {
    for (let i = 0; i < this.list.length; i++) {
      if (this.list[i].id === id) {
        return i;
      }
    }

    return -1;
  }
  getItem(id) {
    const index = this._getIndexById(id);
    if (index < 0) {
      return null;
    }
    return this.list[index];
  }
}

export default ToDoStore;
