import Vue from "vue";
import App from "./App.vue";

import Movue from "movue";
import * as mobx from "mobx";
import Store from "./store";

Vue.use(Movue, mobx);

Vue.prototype.$store = Store;

new Vue({
  el: "#app",
  render: h => h(App)
});
